namespace SeliseBlog1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Id_Type_Changed2 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.Comments");
            AlterColumn("dbo.Comments", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Comments", "Id");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.Comments");
            AlterColumn("dbo.Comments", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Comments", "Id");
        }
    }
}
