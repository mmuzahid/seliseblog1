namespace SeliseBlog1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ID_Generation_Added : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "PostId", "dbo.Posts");
            DropPrimaryKey("dbo.Posts");
            AlterColumn("dbo.Posts", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Posts", "Id");
            AddForeignKey("dbo.Comments", "PostId", "dbo.Posts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "PostId", "dbo.Posts");
            DropPrimaryKey("dbo.Posts");
            AlterColumn("dbo.Posts", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Posts", "Id");
            AddForeignKey("dbo.Comments", "PostId", "dbo.Posts", "Id");
        }
    }
}
