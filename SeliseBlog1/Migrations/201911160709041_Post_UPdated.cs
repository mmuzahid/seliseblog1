namespace SeliseBlog1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Post_UPdated : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "UserId", "dbo.AspNetUsers");
            DropIndex("dbo.Comments", new[] { "UserId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.Comments", "UserId");
            AddForeignKey("dbo.Comments", "UserId", "dbo.AspNetUsers", "Id");
        }
    }
}
