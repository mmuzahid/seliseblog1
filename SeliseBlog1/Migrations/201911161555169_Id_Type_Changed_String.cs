namespace SeliseBlog1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Id_Type_Changed_String : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "PostId", "dbo.Posts");
            DropIndex("dbo.Comments", new[] { "PostId" });
            DropPrimaryKey("dbo.Comments");
            DropPrimaryKey("dbo.Posts");
            AlterColumn("dbo.Comments", "Id", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Comments", "PostId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Posts", "Id", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Comments", "Id");
            AddPrimaryKey("dbo.Posts", "Id");
            CreateIndex("dbo.Comments", "PostId");
            AddForeignKey("dbo.Comments", "PostId", "dbo.Posts", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Comments", "PostId", "dbo.Posts");
            DropIndex("dbo.Comments", new[] { "PostId" });
            DropPrimaryKey("dbo.Posts");
            DropPrimaryKey("dbo.Comments");
            AlterColumn("dbo.Posts", "Id", c => c.Guid(nullable: false));
            AlterColumn("dbo.Comments", "PostId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Comments", "Id", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Posts", "Id");
            AddPrimaryKey("dbo.Comments", "Id");
            CreateIndex("dbo.Comments", "PostId");
            AddForeignKey("dbo.Comments", "PostId", "dbo.Posts", "Id", cascadeDelete: true);
        }
    }
}
