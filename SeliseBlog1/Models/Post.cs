namespace SeliseBlog1.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public class Post
    {
        public string Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }

        [ForeignKey("ApplicationUser")]
        public string UserId { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
