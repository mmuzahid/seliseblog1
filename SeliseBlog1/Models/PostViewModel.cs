﻿using SeliseBlog1.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SeliseBlog1.Models
{
    public class PostViewModel
    {
        public PostViewModel()
        {

        }
        public PostViewModel(Post post)
        {
            AllUsers = new List<SelectListItem>();
            this.Id = post.Id;
            this.Title = post.Title;
            this.Content = post.Content;
            this.CreatedOn = post.CreatedOn;
            this.UpdatedOn = post.UpdatedOn;
            this.UserId = post.UserId;
        }

        public string Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
        public string UserId { get; set; }

        public IList<SelectListItem> AllUsers { get; set; }



    }
}