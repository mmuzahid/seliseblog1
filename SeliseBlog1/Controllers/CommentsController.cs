﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SeliseBlog1.Models;

namespace SeliseBlog1.Controllers
{
    public class CommentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Comments
        public async Task<ActionResult> Index()
        {
            return View(await db.Comments.ToListAsync());
        }

        // GET: Comments/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment Comment = await db.Comments.FindAsync(id);
            if (Comment == null)
            {
                return HttpNotFound();
            }
            return View(Comment);
        }

        // GET: Comments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Comments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Content,CreatedOn,UpdatedOn,PostId,UserId")] Comment Comment)
        {
            if (ModelState.IsValid)
            {
                db.Comments.Add(Comment);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(Comment);
        }

        // GET: Comments/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment Comment = await db.Comments.FindAsync(id);
            if (Comment == null)
            {
                return HttpNotFound();
            }
            return View(Comment);
        }

        // POST: Comments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Content,CreatedOn,UpdatedOn,PostId,UserId")] Comment Comment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(Comment).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(Comment);
        }

        // GET: Comments/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Comment Comment = await db.Comments.FindAsync(id);
            if (Comment == null)
            {
                return HttpNotFound();
            }
            return View(Comment);
        }

        // POST: Comments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            Comment Comment = await db.Comments.FindAsync(id);
            db.Comments.Remove(Comment);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
