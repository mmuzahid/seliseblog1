﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SeliseBlog1.Startup))]
namespace SeliseBlog1
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
